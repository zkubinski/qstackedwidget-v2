QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    engine/allsettings.cpp \
    engine/fileconfig.cpp \
    gui/settingspreview.cpp \
    main.cpp \
    gui\mainwindow.cpp \
    gui\page1.cpp \
    gui\page2.cpp

HEADERS += \
    engine/allsettings.h \
    engine/fileconfig.h \
    gui/settingspreview.h \
    gui\mainwindow.h \
    gui\page1.h \
    gui\page2.h

FORMS += \
    gui\mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
