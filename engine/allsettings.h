#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QJsonObject>

#include <QDebug>

class AllSettings : public QObject
{
    Q_OBJECT

public:
    AllSettings() = default;
    AllSettings(const AllSettings &_settings) = delete;
    AllSettings &operator=(const AllSettings &_settings) = delete;

    QJsonObject ShowSettings();

public slots:
    void CreateNetworkSettings(QJsonObject &networkSettings);
    void CreateDataSettings(QJsonObject &dataSettings);
    void UpdateSettings();

private:
    QJsonObject *createNetworkSettings, *createDataSettings, allSettings;

signals:
    void Settings(QJsonObject &allSettings);
};

#endif // SETTINGS_H
