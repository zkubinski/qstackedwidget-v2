#include "allsettings.h"
#include <QDebug>

void AllSettings::CreateNetworkSettings(QJsonObject &networkSettings)
{
    this->createNetworkSettings = &networkSettings;
}

void AllSettings::CreateDataSettings(QJsonObject &dataSettings)
{
    this->createDataSettings = &dataSettings;
}

void AllSettings::UpdateSettings()
{
    QJsonObject networkSettings, dataSettings, settings;

    networkSettings["network"] = *createNetworkSettings;
    dataSettings["data-from"] = *createDataSettings;

    for(QString &key : networkSettings.keys()){
        settings.insert(key, networkSettings.value(key));
    }
    for(QString &key : dataSettings.keys()){
        settings.insert(key, dataSettings.value(key));
    }

    allSettings["settings"] = settings;

    emit Settings(allSettings);

//kontrolne wypisanie zawartości obiektu allSettings

//    QJsonObject::iterator i;
//    for(i = allSettings.begin(); i != allSettings.end(); ++i){
//        qDebug()<< i.key() << i.value().toString();

//        QJsonObject::iterator j;
//        QJsonObject nextObj1 = i.value().toObject();

//        for(j = nextObj1.begin(); j != nextObj1.end(); ++j){
//            qDebug()<< "\t" << j.key() << j.value().toString();

//            QJsonObject::iterator k;
//            QJsonObject nextObj2 = j.value().toObject();

//            for(k = nextObj2.begin(); k != nextObj2.end(); ++k){
//                qDebug()<< "\t\t" << k.key() << k.value().toString();
//            }
//        }
//    }
}

QJsonObject AllSettings::ShowSettings()
{
    return allSettings;
}
