#ifndef SETTINGSFILE_H
#define SETTINGSFILE_H

#include <QObject>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFile>
#include <QMessageBox>

#include <engine/allsettings.h>

class SettingsFile : public QObject
{
    Q_OBJECT

public:
    SettingsFile() = default;

    QJsonObject *FileSettings();
    void SaveSettings();
    QJsonObject ReadSettings();

public slots:
    void setFileSettingsToSave(QJsonObject &settingsToSave);
    void FileSettingsToRead();

private:
    QJsonObject *settingsToSave;
    QFile settingsToFile;
    QString department, connection, ipAddress, login, password;
    QJsonObject objectJson;
    QMessageBox message;

signals:
//    void DataReadFromFile(QJsonObject &dataFromJsonFile);
};


#endif // SETTINGSFILE_H
