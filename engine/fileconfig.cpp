#include "fileconfig.h"
#include <QDebug>


void SettingsFile::setFileSettingsToSave(QJsonObject &settingsToSave)
{
    this->settingsToSave = &settingsToSave;
}

void SettingsFile::FileSettingsToRead()
{

}

QJsonObject *SettingsFile::FileSettings()
{
    return settingsToSave;
}

void SettingsFile::SaveSettings()
{
    QJsonDocument JsonDocument(*settingsToSave);

    settingsToFile.setFileName(QString("settings.json"));

    if(!settingsToFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate)){
        message.critical(nullptr, QString(tr("Zapis ustawień")), QString(tr("Nie można utworzyć bufora zapisu pliku")), QMessageBox::Ok);
    }
    else{
        settingsToFile.write(JsonDocument.toJson());
        settingsToFile.flush();
        settingsToFile.close();

        message.information(nullptr, QString(tr("Zapis ustawień")), QString(tr("Plik z ustawieniami został zapisany")), QMessageBox::Ok);
    }
}

QJsonObject SettingsFile::ReadSettings()
{
    QFile jsonFile;

    jsonFile.setFileName(QString("settings.json"));

    if(!jsonFile.open(QIODevice::ReadOnly | QIODevice::Text)){
        return objectJson;
    }
    else{
        QByteArray data;
        data = jsonFile.readAll();

        QJsonDocument documentJson;
        QJsonParseError errorData;

        documentJson = QJsonDocument::fromJson(data, &errorData);

        objectJson = documentJson.object();

        return objectJson;
    }
}
