#ifndef PAGE1_H
#define PAGE1_H

#include <QObject>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QComboBox>
#include <QSpacerItem>
#include <QHostAddress>
#include <QLineEdit>
#include <QJsonObject>
#include <QCryptographicHash>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QValidator>

#include <QDebug>

class Page1 : public QWidget
{
    Q_OBJECT

public:
    Page1(QWidget *parent = nullptr);
    Page1(const Page1 &page) = delete;
    Page1 &operator=(const Page1 &page) = delete;

public slots:
    void ShowNetworkSettings();
    void ClearFields();

private slots:
    void setLogin(const QString &login);
    void setPassword(const QString &password);
    void setConnectType(const QString &selectedText);
    void setIPAdress(const QString &ipAddress);
    void setActiveIndex(int index);

private:
    QVBoxLayout *mainLayout;
    QHBoxLayout *layoutLogin, *layoutPassword, *layoutConnectType, *layoutIPAdress;
    QLabel *labelLogin, *labelPassword, *labelConnectDatabase, *labelConnectType, *labelIPAdress;
    QComboBox *comboBox;
    QSpacerItem *spacer;
    QHostAddress *ipv4Database;
    QLineEdit *login, *password, *ipDatabase;
    QString ipv4;
    QJsonObject jsonLogin, jsonPassword, jsonIPAddress, jsonConnectType, jsonNetworkSettings;

    QRegularExpression regexIP;
    QRegularExpressionMatch matchIP;
    QValidator *validateIP;

signals:
    void NetworkSettings(QJsonObject &networkSettings);
    void Active(bool);
};

#endif // PAGE1_H
