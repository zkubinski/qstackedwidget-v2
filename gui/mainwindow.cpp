#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)/*, ui(new Ui::MainWindow)*/
{
//    ui->setupUi(this);
    this->resize(600,250);

    mainWidget = new QWidget();
    mainLayout = new QVBoxLayout(mainWidget);

    fontOptions = new QFont();
    fontOptions->setWeight(QFont::Bold);
    fontOptions->setPointSize(10);

    brushFont = new QBrush(QColor(Qt::blue));
    brushFont->setStyle(Qt::SolidPattern);

    brushFont2 = new QBrush(QColor(Qt::white));
    brushFont2->setStyle(Qt::SolidPattern);

    fontColor = new QPalette();
//    FontColor->setBrush(QPalette::Active, QPalette::WindowText, *BrushFont);
    fontColor->setBrush(QPalette::Active, QPalette::Window, *brushFont2);
//    FontColor->setColor(foregroundRole(), Qt::blue);

    label1 = new QLabel();
    label1->setFont(*fontOptions);
    label1->setPalette(*fontColor);
    label1->setLineWidth(1);
    label1->setMidLineWidth(0);
    label1->setFrameStyle(QFrame::Box | QFrame::Plain);
    label1->setText(QString(tr("Opcje")));
    label1->setAlignment(Qt::AlignCenter);
    label1->setAutoFillBackground(true);

    label2 = new QLabel();
    label2->setFont(*fontOptions);
    label2->setPalette(*fontColor);
    label2->setLineWidth(1);
    label2->setMidLineWidth(0);
    label2->setFrameStyle(QFrame::Box | QFrame::Plain);
    label2->setText(QString(tr("Ustawienia")));
    label2->setAlignment(Qt::AlignCenter);
    label2->setAutoFillBackground(true);

    layoutForLabel = new QHBoxLayout();
    layoutForLabel->addWidget(label1);
    layoutForLabel->addWidget(label2);

    layoutForStackedWidget = new QHBoxLayout();

    myPage1 = new Page1();
    myPage2 = new Page2();
    settingsPreview = new SettingsPreview(this);

    optionList = new QListWidget(this);
    optionList->addItem(QString(tr("Sieć")));
    optionList->addItem(QString(tr("Dane")));
    optionList->setCurrentRow(0);

    stackedWidget = new QStackedWidget();
    stackedWidget->addWidget(myPage1);
    stackedWidget->addWidget(myPage2);

    layoutForButton = new QHBoxLayout();

    pbSave = new QPushButton(this);
    pbSave->setText(QString(tr("Zapisz ustawienia")));

    pbApply = new QPushButton(this);
    pbApply->setText(QString(tr("Zastosuj")));

    pbShowSettings = new QPushButton(this);
    pbShowSettings->setText(QString(tr("Pokaż ustawienia")));

    QAction *clear, *readSettings;

    pbPopupOptions = new QPushButton(this);
    pbPopupOptions->setText(QString(tr("Dodatkowe opcje")));
    pbClearMenu = new QMenu(this);
    clear = pbClearMenu->addAction(QString(tr("Wyczyść wszystkie pola")));
    readSettings = pbClearMenu->addAction(QString(tr("Wczytaj ustawienia")));
    pbPopupOptions->setMenu(pbClearMenu);

    pbExit = new QPushButton(this);
    pbExit->setText(QString(tr("Zamknij")));

    layoutForButton->addWidget(pbSave);
    layoutForButton->addWidget(pbApply);
    layoutForButton->addWidget(pbShowSettings);
    layoutForButton->addWidget(pbPopupOptions);
    layoutForButton->addWidget(pbExit);

    layoutForStackedWidget->addWidget(optionList);
    layoutForStackedWidget->addWidget(stackedWidget);

    mainLayout->addLayout(layoutForLabel);
    mainLayout->addLayout(layoutForStackedWidget);
    mainLayout->addLayout(layoutForButton);

    this->setCentralWidget(mainWidget);
//    StateReadJsonFile(mySettingsFile.ReadSettings());
    DataFromJsonFile(mySettingsFile.ReadSettings());
    QObject::connect(optionList, &QListWidget::currentRowChanged, stackedWidget, &QStackedWidget::setCurrentIndex);
    QObject::connect(pbExit, &QPushButton::clicked, this, &MainWindow::close);

    QObject::connect(myPage1, &Page1::NetworkSettings, &mySettings, &AllSettings::CreateNetworkSettings);
    QObject::connect(pbSave, &QPushButton::clicked, myPage1, &Page1::ShowNetworkSettings);

    QObject::connect(myPage2, &Page2::dataSettings, &mySettings, &AllSettings::CreateDataSettings);
    QObject::connect(pbSave, &QPushButton::clicked, myPage2, &Page2::ShowDataSettings);

    QObject::connect(pbShowSettings, &QPushButton::clicked, settingsPreview, &QDialog::exec);

    QObject::connect(clear, &QAction::triggered, this, &MainWindow::ClearEdit);
    QObject::connect(readSettings, &QAction::triggered, this, &MainWindow::ReadSettings);

    QObject::connect(pbSave, &QPushButton::clicked, &mySettings, &AllSettings::UpdateSettings);

    QObject::connect(&mySettings, &AllSettings::Settings, &mySettingsFile, &SettingsFile::setFileSettingsToSave);
    QObject::connect(pbSave, &QPushButton::clicked, &mySettingsFile, &SettingsFile::SaveSettings);
}

void MainWindow::ClearEdit()
{
    if(stackedWidget->currentIndex() == 0){
        myPage1->ClearFields();
    }
    else if(stackedWidget->currentIndex() == 1){
        myPage2->ClearFields();
    }
}

void MainWindow::DataFromJsonFile(const QJsonObject &data)
{
    if(data.isEmpty()){
        qDebug()<< "pusty";
    }
    else if(!data.isEmpty()){

    }
}

void MainWindow::ReadSettings()
{
    qDebug()<< "wczytuje";
}

MainWindow::~MainWindow()
{
//    delete mySettings;
//    delete myJsonObject;
//    delete mySettingsFile;
    //    delete ui;
}

