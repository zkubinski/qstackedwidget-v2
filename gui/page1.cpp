#include "page1.h"
#include <QDebug>

Page1::Page1(QWidget *parent) : QWidget(parent)
{
    mainLayout = new QVBoxLayout(this);
    mainLayout->setAlignment(Qt::AlignTop);

    labelConnectDatabase = new QLabel(this);
    labelConnectDatabase->setText(QString(tr("Połączenie z bazą")));
    labelConnectDatabase->setAlignment(Qt::AlignCenter);

    spacer = new QSpacerItem(0,10);
//-------------------Login-------------------------------
    layoutLogin = new QHBoxLayout();

    labelLogin = new QLabel(this);
    labelLogin->setText(QString(tr("Login")));

    login = new QLineEdit(this);
    login->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
//-------------------END Login---------------------------

//-------------------Password----------------------------
    layoutPassword = new QHBoxLayout();

    labelPassword = new QLabel(this);
    labelPassword->setText(QString(tr("Hasło")));

    password = new QLineEdit(this);
    password->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    password->setEchoMode(QLineEdit::Password);
 //-------------------END Password-----------------------

//-------------------Connect type------------------------
    layoutConnectType = new QHBoxLayout();
    layoutConnectType->setAlignment(Qt::AlignTop);

    labelConnectType = new QLabel(this);
    labelConnectType->setText(QString(tr("Typ połączenia")));
//-------------------END Connect type--------------------
    comboBox = new QComboBox(this);
    comboBox->addItem(QString(tr("Komputer lokalny")));
    comboBox->addItem(QString(tr("Komputer zdalny")));
    comboBox->setCurrentIndex(0);

    if(comboBox->currentIndex() == 0){
        this->jsonConnectType["connect"] = comboBox->currentText();
    }
    if(comboBox->currentIndex() == 1){
        this->jsonConnectType["connect"] = comboBox->currentText();
    }
//-------------------IP Address-------------------------
    layoutIPAdress = new QHBoxLayout();

    labelIPAdress = new QLabel(this);
    labelIPAdress->setText(QString(tr("Adres IP")));
//------------------ END IP Address---------------------
    regexIP.setPattern(QString(""));
    validateIP = new QRegularExpressionValidator(regexIP, this);

    ipDatabase = new QLineEdit(this);
    ipDatabase->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    ipDatabase->setText("127.0.0.1");
    ipDatabase->setEnabled(false);

    if(ipDatabase->text().isEmpty()){
        this->jsonIPAddress["ipaddr"] = ipDatabase->text();
    }
    if(!ipDatabase->text().isEmpty()){
        this->jsonIPAddress["ipaddr"] = ipDatabase->text();
    }

    ipv4Database = new QHostAddress();
    ipv4Database->setAddress(ipDatabase->text());

    mainLayout->addWidget(labelConnectDatabase);
    mainLayout->addItem(spacer);

    layoutLogin->addWidget(labelLogin);
    layoutLogin->addWidget(login);

    layoutPassword->addWidget(labelPassword);
    layoutPassword->addWidget(password);

    layoutConnectType->addWidget(labelConnectType);
    layoutConnectType->addWidget(comboBox);

    layoutIPAdress->addWidget(labelIPAdress);
    layoutIPAdress->addWidget(ipDatabase);

    mainLayout->addLayout(layoutLogin);
    mainLayout->addLayout(layoutPassword);
    mainLayout->addLayout(layoutConnectType);
    mainLayout->addLayout(layoutIPAdress);

    QObject::connect(login, &QLineEdit::textChanged, this, &Page1::setLogin);
    QObject::connect(password, &QLineEdit::textChanged, this, &Page1::setPassword);
    QObject::connect(comboBox, &QComboBox::currentTextChanged, this, &Page1::setConnectType);
    QObject::connect(comboBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &Page1::setActiveIndex);
    QObject::connect(this, &Page1::Active, ipDatabase, &QLineEdit::setEnabled);
    QObject::connect(ipDatabase, &QLineEdit::textChanged, this, &Page1::setIPAdress);
}

void Page1::setLogin(const QString &login)
{
    this->jsonLogin["login"] = login;
}

void Page1::setPassword(const QString &password)
{
    QCryptographicHash passwordCrypt(QCryptographicHash::Sha3_256);

    passwordCrypt.addData(password.toUtf8());

    QString crypt(passwordCrypt.result().toHex());

    this->jsonPassword["password"] = crypt;
}

void Page1::setConnectType(const QString &selectedText)
{
    this->jsonConnectType["connect"] = selectedText;
}

void Page1::setIPAdress(const QString &ipAddress)
{
    this->jsonIPAddress["ipaddr"] = ipAddress;
}

void Page1::setActiveIndex(int index)
{
    if(index == 0){
        if(ipDatabase->text().isEmpty() == true){
            ipDatabase->setText("127.0.0.1");
        }
        else if(ipDatabase->text() != "127.0.0.1"){
            ipDatabase->setText("127.0.0.1");
        }

        emit Active(false);
    }
    else if(index == 1){
        emit Active(true);
    }
}

void Page1::ShowNetworkSettings()
{
    QJsonObject settings;

    for(QString &keyRow1 : jsonLogin.keys()){
        settings.insert(keyRow1, jsonLogin.value(keyRow1).toString());
    }
    for(QString &keyRow2 : jsonPassword.keys()){
        settings.insert(keyRow2, jsonPassword.value(keyRow2).toString());
    }
    for(QString &keyRow3 : jsonConnectType.keys()){
        settings.insert(keyRow3, jsonConnectType.value(keyRow3).toString());
    }
    for(QString &keyRow4 : jsonIPAddress.keys()){
        settings.insert(keyRow4, jsonIPAddress.value(keyRow4).toString());
    }

    jsonNetworkSettings/*["network"]*/ = settings;

//    QJsonObject::iterator i;

//    for(i = networkSettings.begin(); i != networkSettings.end(); ++i){
//        qDebug()<< i.key() << networkSettings.value(i.key()).toString();

//        QJsonObject::iterator j;
//        QJsonObject nextObj = i.value().toObject();

//        for(j = nextObj.begin(); j != nextObj.end(); ++j){
//            qDebug()<< "\t" << j.key() << ":" << nextObj.value(j.key()).toString();
//        }
//    }

    emit NetworkSettings(jsonNetworkSettings);
}

void Page1::ClearFields()
{
    if(login->text().isEmpty() == false){
        login->clear();
    }

    if(password->text().isEmpty() == false){
        password->clear();
    }

    if(ipDatabase->isEnabled() == true){
        ipDatabase->clear();
    }
}
