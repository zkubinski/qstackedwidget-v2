#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QWidget>
#include <QStackedWidget>
#include <QListWidget>
#include <QLabel>
#include <QFont>
#include <QPalette>
#include <QBrush>
#include <QPushButton>
#include <QMenu>
#include <QAction>
#include <QJsonDocument>
#include <QMessageBox>

#include "page1.h"
#include "page2.h"
#include "gui/settingspreview.h"

#include "engine/allsettings.h"
#include "engine/fileconfig.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QHBoxLayout *layoutForLabel, *layoutForButton;
    QLabel *label1, *label2;
    QFont *fontOptions;
    QPalette *fontColor, *fontColor2;
    QBrush *brushFont, *brushFont2;

    QWidget *mainWidget;
    QVBoxLayout *mainLayout;
    QStackedWidget *stackedWidget;
    QListWidget *optionList;
    QMenu *pbClearMenu;
    QPushButton *pbSave, *pbApply, *pbShowSettings, *pbPopupOptions, *pbExit;

    QHBoxLayout *layoutForStackedWidget;

    Page1 *myPage1;
    Page2 *myPage2;
    SettingsPreview *settingsPreview;

    AllSettings mySettings;
    SettingsFile mySettingsFile;

    QMessageBox messageError;

//    void StateReadJsonFile(const int &state);
    void DataFromJsonFile(const QJsonObject &data);

private slots:
    void ClearEdit();
    void ReadSettings();

//    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
