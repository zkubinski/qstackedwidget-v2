#ifndef SETTINGSPREVIEW_H
#define SETTINGSPREVIEW_H

#include <QObject>
#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QFont>
#include <QBrush>
#include <QPalette>
#include <QTextBrowser>
#include <QPushButton>

#include "engine/allsettings.h"

class SettingsPreview : public QDialog
{
    Q_OBJECT
public:
    explicit SettingsPreview(QWidget *parent = nullptr);

private:
    QVBoxLayout *mainLayout;
    QHBoxLayout *pbLayout;
    QLabel *labelInformation;
    QFont *fontInformation;
    QBrush *brushInformation;
    QPalette *paletteInformation;
    QTextBrowser *showSettings;
    QPushButton *pbExit;

signals:

};

#endif // SETTINGSPREVIEW_H
