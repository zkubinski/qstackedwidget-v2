#ifndef PAGE2_H
#define PAGE2_H

#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QSpacerItem>
#include <QComboBox>
#include <QJsonObject>

class Page2 : public QWidget
{
    Q_OBJECT

public:
    Page2(QWidget *parent = nullptr);
    Page2(const Page2 &page) = delete;
    Page2 &operator=(const Page2 &page) = delete;

public slots:
    void ShowDataSettings();
    void ClearFields();

private slots:
    void setDept(const QString &dept);

private:
    QVBoxLayout *MainLayout;
    QHBoxLayout *Layout, *Layout2;
    QLabel *Label, *Label2;
    QSpacerItem *Spacer;
    QComboBox *ComboBox;
    QJsonObject department, data_from;

signals:
    void dataSettings(QJsonObject &dataSettings);
};

#endif // PAGE2_H
