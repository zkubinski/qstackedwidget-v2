#include "settingspreview.h"
#include <QDebug>

SettingsPreview::SettingsPreview(QWidget *parent) : QDialog(parent)
{
    mainLayout = new QVBoxLayout(this);

    fontInformation = new QFont();
    fontInformation->setWeight(QFont::Bold);
    fontInformation->setPointSize(10);

    brushInformation = new QBrush(QColor(Qt::white));
    brushInformation->setStyle(Qt::SolidPattern);

    paletteInformation = new QPalette();
    paletteInformation->setBrush(QPalette::Active, QPalette::Window, *brushInformation);

    labelInformation = new QLabel(this);
    labelInformation->setFont(*fontInformation);
    labelInformation->setPalette(*paletteInformation);
    labelInformation->setLineWidth(1);
    labelInformation->setMidLineWidth(0);
    labelInformation->setFrameStyle(QFrame::Box | QFrame::Plain);
    labelInformation->setText(QString(tr("Podgląd ustawień")));
    labelInformation->setAlignment(Qt::AlignCenter);
    labelInformation->setAutoFillBackground(true);

    showSettings = new QTextBrowser();
    showSettings->setText("{\n  \"settings\":{\n    \"data-from\":");

    pbLayout = new QHBoxLayout();

    pbExit = new QPushButton(this);
    pbExit->setText(QString(tr("Zamknij")));

    pbLayout->addWidget(pbExit);

    mainLayout->addWidget(labelInformation);
    mainLayout->addWidget(showSettings);
    mainLayout->addLayout(pbLayout);

    QObject::connect(pbExit, &QPushButton::clicked, this, &QWidget::close);
}
