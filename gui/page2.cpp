#include "page2.h"

#include <QDebug>

Page2::Page2(QWidget *parent) : QWidget(parent)
{
    MainLayout = new QVBoxLayout(this);
    MainLayout->setAlignment(Qt::AlignTop);

    Label = new QLabel(this);
    Label->setText(QString(tr("Pobierz dane z")));
    Label->setAlignment(Qt::AlignCenter);

    Layout = new QHBoxLayout();

    Label2 = new QLabel(this);
    Label2->setText(QString(tr("Wydział")));

    Spacer = new QSpacerItem(0,10);

    ComboBox = new QComboBox(this);
    ComboBox->addItem("1 Cywilny");
    ComboBox->addItem("2 Cywilny");
    ComboBox->addItem("3 Karny");
    ComboBox->addItem("4 Karny");
    ComboBox->setCurrentIndex(0);

    if(ComboBox->currentIndex() == 0){
        this->department["dept"] = ComboBox->currentText();
    }

    Layout->addWidget(Label2);
    Layout->addWidget(ComboBox);

    MainLayout->addWidget(Label);
    MainLayout->addItem(Spacer);
    MainLayout->addLayout(Layout);

    QObject::connect(ComboBox, &QComboBox::currentTextChanged, this, &Page2::setDept);
}

void Page2::setDept(const QString &department)
{
    this->department["dept"] = department;
}

void Page2::ShowDataSettings()
{
    QJsonObject settings;

    for(QString &keyRow1 : department.keys()){
        settings.insert(keyRow1, department.value(keyRow1).toString());
    }

    this->data_from/*["data-from"]*/ = settings;

    emit dataSettings(data_from);
}

void Page2::ClearFields()
{
    qDebug()<< "jeszcze nic nie ma do czyszczenia";
}
